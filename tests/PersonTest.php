<?php

use PHPUnit\Framework\TestCase;

/**
 * Test class of the Person class.
 *
 * @inheritDoc
 */
class PersonTest extends TestCase {

  /**
   * Instantiates the Person class.
   */
  public function setUp(): void {
    $this->Person = new Person("John Doe", 37, "Male");
  }

  /**
   * Tests the constructor function.
   */
  public function testConstructor() {
    // $this->Person = new Person("John Doe", 37, "Male");
    $this->assertEquals("John Doe", $this->Person->getName());
    $this->assertEquals(37, $this->Person->getAge());
    $this->assertEquals("Male", $this->Person->getGenre());
  }

  /**
   * Tests the birthday function.
   */
  public function testBirthday() {
    $this->assertEquals(($this->Person->getAge() + 1), $this->Person->birthday());
  }

}
