<?php

use PHPUnit\Framework\TestCase;

/**
 * Test class of the Book class.
 *
 * @inheritDoc
 */
class BookTest extends TestCase {

  /**
   * Instantiates the Person class.
   */
  public function setUp(): void {
    $this->Person = new Person("John Doe", 37, "Male");
    $this->book = new Book("O Hobbit", "J.R.R Tolkien", 300, $this->Person->getName());
    $this->book2 = new Book("O Senhor dos Aneis - O Retorno do Rei", "J.R.R Tolkien", 527, $this->Person->getName());
  }

  /**
   * Tests the constructor function.
   */
  public function testConstructor() {
    $this->assertEquals("O Hobbit", $this->book->getTitle());
    $this->assertEquals("J.R.R Tolkien", $this->book->getAuthor());
    $this->assertEquals(300, $this->book->getTotalPages());
    $this->assertEquals("John Doe", $this->Person->getName());
  }

  /**
   * Tests the open function.
   */
  public function testOpen() {
    $this->assertEquals(TRUE, $this->book->open());
  }

  /**
   * Tests the close function.
   */
  public function testClose() {
    $this->assertEquals(FALSE, $this->book->close());
  }

  /**
   * Tests the browse function.
   */
  public function testBrowse() {
    $this->assertEquals(0, $this->book->browse(301));
    $this->assertEquals(100, $this->book->browse(100));
    $this->assertEquals(0, $this->book->browse(-1));

  }

  /**
   * Tests the jumpFoward function.
   */
  public function testJumpFoward() {
    $this->book->setPage(300);
    $this->assertEquals("You reached the end of the book!", $this->book->jumpFoward());
    $this->book2->setPage($this->book->getTotalPages() - 5);
    $this->assertEquals(($this->book2->getPage() + 1), $this->book2->jumpFoward());
  }

  /**
   * Tests the jumpBackward function.
   */
  public function testJumpBackWard() {
    $this->book->setPage(1);
    $this->assertEquals("You are in the first page of the book!", $this->book->jumpBackWard());
    $this->book2->setPage(200);
    $this->assertEquals(($this->book2->getPage() - 1), $this->book2->jumpBackWard());

  }

}
