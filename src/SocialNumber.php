<?php

/**
 * Validade the social number.
 *
 * @inheritDoc
 */
class SocialNumber {

  /**
   * Return a type of a triangle.
   *
   * @param string $socialNumber
   *   Receive a string with the social number do be validated.
   *
   * @return bool
   *   Return a bool with the validation
   */
  public static function validateSocialNumber(string $socialNumber): bool {
    if (!(preg_match('/[0-9]{11}/', $socialNumber)) || (preg_match('/([0-9])\1{5}/', $socialNumber))) {
      return FALSE;
    }

    $firstDigitVal = (
      ((int) substr($socialNumber, 0, 1) * 10) +
      ((int) substr($socialNumber, 1, 1) * 9) +
      ((int) substr($socialNumber, 2, 1) * 8) +
      ((int) substr($socialNumber, 3, 1) * 7) +
      ((int) substr($socialNumber, 4, 1) * 6) +
      ((int) substr($socialNumber, 5, 1) * 5) +
      ((int) substr($socialNumber, 6, 1) * 4) +
      ((int) substr($socialNumber, 7, 1) * 3) +
      ((int) substr($socialNumber, 8, 1) * 2)
    );

    $restOne = (($firstDigitVal * 10) % 11);
    if ($restOne != substr($socialNumber, 9, 1)) {
      return FALSE;
    }

    $secondDigitVal = (
      ((int) substr($socialNumber, 0, 1) * 11) +
      ((int) substr($socialNumber, 1, 1) * 10) +
      ((int) substr($socialNumber, 2, 1) * 9) +
      ((int) substr($socialNumber, 3, 1) * 8) +
      ((int) substr($socialNumber, 4, 1) * 7) +
      ((int) substr($socialNumber, 5, 1) * 6) +
      ((int) substr($socialNumber, 6, 1) * 5) +
      ((int) substr($socialNumber, 7, 1) * 4) +
      ((int) substr($socialNumber, 8, 1) * 3) +
      ((int) substr($socialNumber, 9, 1) * 2)
    );

    $restTwo = (($secondDigitVal * 10) % 11);
    if ($restTwo != substr($socialNumber, 10, 1)) {
      return FALSE;
    }

    return TRUE;
  }

}
