<?php

/**
 * Find a type of a triangle based on a given side lenght.
 *
 * @inheritDoc
 */
class Triangle {

  /**
   * Return a type of a triangle.
   *
   * @param array $numbers
   *   Receive the values of the lenght of the sides of the triangle.
   *
   * @return string
   *   Return the type of the triangle: scalene, equilateral or isosceles.
   */
  public static function triangleType(array $numbers): String {
    if ($numbers[0] == $numbers[1] && $numbers[1] == $numbers[2]) {
      return "Equilateral";
    }

    if ($numbers[0] == $numbers[1] || $numbers[1] == $numbers[2] || $numbers[0] == $numbers[2]) {
      return "Isosceles";
    }
    else {
      return "Scalane";
    }
  }

}
