<?php

/**
 * Book's publication.
 */
interface PublicationInterface {

  /**
   * Opens the book.
   *
   * @return bool
   *   Returns the book opened.
   */
  public function open();

  /**
   * Closes the book.
   *
   * @return bool
   *   Returns the book closed.
   */
  public function close();

  /**
   * Jumps forward.
   *
   * @return int|string
   *   Returns the result of jumping forward
   */
  public function jumpFoward();

  /**
   * Jumps backward.
   *
   * @return int|string
   *   Returns the result of jumping backward
   */
  public function jumpBackWard();

}
