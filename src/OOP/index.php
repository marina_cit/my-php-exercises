<?php

/**
 * @file
 * Instantiates and writes Book and Person classes information.
 */

require_once 'Person.php';
require_once 'Book.php';

$books = [];
$people = [];

$people[0] = new Person("Marina", 26, "Female");
$people[1] = new Person("João", 33, "Male");
$people[3] = new Person("Livia", 16, "Female");

$books[0] = new Book("The Diary of a Young Girl", "Anne Frank", 222, $people[0]);
$books[1] = new Book("The Perks of Being a Wallflower", "Stephen Chbosky", 432, $people[3]);
$books[2] = new Book("The Book Thief", "Marcus Zusak", 543, $people[0]);
$books[3] = new Book("The Theory of Everything", "Stephen Hawking", 787, $people[1]);


$books[0]->open();
$books[0]->setPage(23);
$books[3]->open();
$books[3]->setPage(55);

?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bookstore</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

    <h1 style="text-align: center;">Bookstore</h1>

    <?php foreach ($books as $book) {
      $book->details();
      echo "<hr>";
    } ?>


</body>

</html>
