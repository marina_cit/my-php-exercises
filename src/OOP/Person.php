<?php

/**
 * This class provides Person's information.
 */
class Person {

  /**
   * Person's name.
   *
   * @var string
   */
  private $name;

  /**
   * Person's age.
   *
   * @var int
   */
  private $age;

  /**
   * Person's genre.
   *
   * @var string
   */
  private $genre;

  /**
   * Class constructor.
   *
   * @param string $name
   *   Person's name.
   * @param int $age
   *   Person's age.
   * @param string $genre
   *   Person's genre.
   */
  public function __construct($name, $age, $genre) {
    $this->name = $name;
    $this->age = $age;
    $this->genre = $genre;
  }

  /**
   * Gets name.
   *
   * @return string
   *   Returns $name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Gets age.
   *
   * @return int
   *   Returns $age.
   */
  public function getAge() {
    return $this->age;
  }

  /**
   * Gets genre.
   *
   * @return string
   *   Returns $genre.
   */
  public function getGenre() {
    return $this->genre;
  }

  /**
   * Sets the name.
   *
   * @param string $name
   *   Sets the name.
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Sets the age.
   *
   * @param int $age
   *   Sets the age.
   */
  public function setAge($age) {
    $this->age = $age;
  }

  /**
   * Sets the genre.
   *
   * @param string $genre
   *   Sets the genre.
   */
  public function setGenre($genre) {
    $this->genre = $genre;
  }

  /**
   * Calculate Person's age after birthday.
   *
   * @return int
   *   Returns the age after birthday.
   */
  public function birthday() {
    return $this->age + 1;
  }

}
