<?php

require_once 'Person.php';
require_once 'PublicationInterface.php';

/**
 * This class provides book's information.
 */
class Book implements PublicationInterface {

  /**
   * Book's title.
   *
   * @var string
   */
  private $title;

  /**
   * Book's author.
   *
   * @var string
   */
  private $author;

  /**
   * Book's total of pages.
   *
   * @var int
   */
  private $totalPages;

  /**
   * Book's page.
   *
   * @var int
   */
  private $page;

  /**
   * Return if the book is open or not.
   *
   * @var bool
   */
  private $open;

  /**
   * Book's reader.
   *
   * @var string
   */
  private $reader;

  /**
   * Class constructor.
   *
   * @param int $title
   *   Book's title.
   * @param string $author
   *   Book's author.
   * @param int $totalPages
   *   Book's total of pages.
   * @param string $reader
   *   Book's reader.
   */
  public function __construct($title, $author, $totalPages, $reader) {
    $this->title = $title;
    $this->author = $author;
    $this->totalPages = $totalPages;
    $this->page = 0;
    $this->open = FALSE;
    $this->reader = $reader;
  }

  /**
   * Writes book's information.
   */
  public function details() {
    echo "<h3>Book's information</h3>";
    echo "<pre>";
    echo "Title: " . $this->title;
    echo "</br>";
    echo "Author: " . $this->author;
    echo "</br>";
    echo "Total of pages: " . $this->totalPages;
    echo "</br>";
    echo "Reader: " . $this->reader->getName();
    echo "</br>";
    echo "This book is " . ($this->open ? "open, current page: " . $this->page : "not open");
    echo "</pre>";
  }

  /**
   * Gets title.
   *
   * @return string
   *   Returns $title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Gets the author.
   *
   * @return string
   *   Returns $author.
   */
  public function getAuthor() {
    return $this->author;
  }

  /**
   * Gets total of pages.
   *
   * @return int
   *   Returns $totalPages.
   */
  public function getTotalPages() {
    return $this->totalPages;
  }

  /**
   * Gets page.
   *
   * @return int
   *   Returns $page.
   */
  public function getpage() {
    return $this->page;
  }

  /**
   * Gets the reader.
   *
   * @return string
   *   Returns $reader.
   */
  public function getreader() {
    return $this->reader;
  }

  /**
   * Sets the author.
   *
   * @param string $author
   *   Sets the author.
   */
  public function setAuthor($author) {
    $this->author = $author;
  }

  /**
   * Sets the title.
   *
   * @param string $title
   *   Sets the title.
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Sets the total of pages.
   *
   * @param int $totalPages
   *   Sets the total of pages.
   */
  public function setTotalPages($totalPages) {
    $this->totalPages = $totalPages;
  }

  /**
   * Sets the page.
   *
   * @param int $page
   *   Sets the page.
   */
  public function setpage($page) {
    $this->page = $page;
  }

  /**
   * Opens the book.
   *
   * @return bool
   *   Returns the book opened.
   */
  public function open() {
    $this->open = TRUE;

    return $this->open;
  }

  /**
   * Closes the book.
   *
   * @return bool
   *   Returns the book closed.
   */
  public function close() {
    $this->open = FALSE;

    return $this->open;
  }

  /**
   * Browses the page.
   *
   * @param int $page
   *   Book's page.
   *
   * @return int
   *   Returns the browse result.
   */
  public function browse($page) {

    if ($page < 0 || $page > $this->totalPages) {
      return 0;
    }

    return $page;
  }

  /**
   * Jumps forward.
   *
   * @return int|string
   *   Returns the result of jumping forward
   */
  public function jumpFoward() {
    if ($this->page++ >= $this->totalPages) {
      return "You reached the end of the book!";
    }
    else {
      return $this->page++;
    }
  }

  /**
   * Jumps backward.
   *
   * @return int|string
   *   Returns the result of jumping backward
   */
  public function jumpBackWard() {
    if ($this->page-- <= 1) {
      return "You are in the first page of the book!";
    }
    else {
      return $this->page--;
    }
  }

}
